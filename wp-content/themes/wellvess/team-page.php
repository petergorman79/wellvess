<?php 

/*
 * Template Name: Team
 * Description: Wellvess team page
 */

 get_header(); ?>
<div class="container sub-page">
	<div class="row page-title">
		<div class="col-xs-12">
			<h1><?php the_title(); ?></h1> <?php edit_post_link(); ?></h1>
			<hr>
		</div> <!-- /Col -->
		
	</div> <!-- /Row -->

	<div class="row">
		<section class="col-md-9 content">

			<?php the_content(); ?>

			<div class="row team">

				<?php

				if( have_rows('team_members') ):

				    while ( have_rows('team_members') ) : the_row(); ?>

						<div class="col-xs-12 member">
							<img src="<?php the_sub_field('team_image'); ?>" />
							<div class="content">
								<h2><?php the_sub_field('team_name'); ?> <span><?php the_sub_field('team_role'); ?></span></h2>
								<p>
									<?php the_sub_field('team_profile'); ?>
								</p>
							</div> <!-- /Content -->
						</div> <!-- /Col -->

				    <?php endwhile;

				else :

				endif;

				?>
				
			</div> <!-- /Row -->

		</section> <!-- /Col -->
		<aside class="col-md-3">

			<?php get_sidebar(); ?>

		</aside> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>