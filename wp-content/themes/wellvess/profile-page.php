<?php 

/*
 * Template Name: My profile
 * Description: Wellvess profile
 */

 get_header(); ?>
<div class="container sub-page">

<?php if ( is_user_logged_in() ) { ?>
	<div class="row page-title">
		<div class="col-xs-12">
			<h1><?php the_title(); ?></h1> <?php edit_post_link(); ?></h1>
			<hr>
		</div> <!-- /Col -->
		
	</div> <!-- /Row -->
<?php } else { ?>

<?php } ?>

	<div class="row">
		<section class="col-md-9 content">

			<?php the_content(); ?>	

		</section> <!-- /Col -->
		<aside class="col-md-3">
		
			<?php get_sidebar(); ?>
		
		</aside> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>