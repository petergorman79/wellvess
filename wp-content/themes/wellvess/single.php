<?php get_header(); ?>

<div class="container sub-page">
	<div class="row page-title">
		<div class="col-xs-12">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'entry' ); ?>
			<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
			<?php endwhile; endif; ?>
		</div> <!-- /Col -->
	</div> <!-- /Row -->

	<div class="row page-title">
		<div class="col-xs-12">
			<?php get_template_part( 'nav', 'below-single' ); ?>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>