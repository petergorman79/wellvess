<?php get_header(); ?>

<div class="container sub-page payment">
	<div class="row page-title">
		<div class="col-xs-12">
			<?php the_post(); ?>
			<h1 class="entry-title author"><?php _e( 'Author Archives', 'blankslate' ); ?>: <?php the_author_link(); ?></h1>
			<?php if ( '' != get_the_author_meta( 'user_description' ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . get_the_author_meta( 'user_description' ) . '</div>' ); ?>
			<?php rewind_posts(); ?>
			<hr>
		</div> <!-- /Col -->
		
	</div> <!-- /Row -->

	<div class="row">
		<section class="col-xs-12 content">

			<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'entry' ); ?>
			<?php endwhile; ?>
			<?php get_template_part( 'nav', 'below' ); ?>

		</section> <!-- /Col -->
 		<!--<aside class="col-md-3">
			<?php get_sidebar(); ?>
		</aside> /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>