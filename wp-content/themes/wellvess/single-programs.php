<?php
/*
Single Post Template:Programs
*/

get_header(); ?>

<?php get_header( 'member' ); ?>

<?php get_header( 'program_weeks' ); ?>

<?php $weeks = get_field('program_weeks'); ?> <!-- Gets the weeks from the program relationship --> 
    <?php if( $weeks ): ?>

		<?php foreach( $weeks as $weeks ): ?> <!-- Sets loop -->

			<?php if ( get_post_status ( $weeks->ID ) == 'publish' ) { ?> <!-- Checks the post status - if published display below -->

					<div class="container">
						<div class="row">

							<div class="col-md-6 col-md-push-6">
		                    	<?php
								$append = week_;
								$my_date = date( 'l', current_time( 'timestamp', 0 ) );
								$archive_day = $append.$my_date ?> <!-- Creates day of the week for relationship --> 

								<?php $recipes = get_field( $archive_day , $weeks->ID ); ?> <!-- Gets the recipe for the current day --> 
								
								<?php if( $recipes ):
								$category = get_the_category();
								$firstCategory = $category[0]->cat_name; ?>
									<section class="food-tile">
										<h2>Todays meal plan</h2>
									<ul>
									<?php foreach( $recipes as $r ): // variable must NOT be called $post (IMPORTANT) ?>
									    <a href="<?php echo get_permalink( $r->ID ); ?>">
									    	<li>
									    		<img src="<?php the_field('recipe_image', $r->ID); ?>" alt="">
									    		<h5><?php echo get_the_title( $r->ID ); ?></h5>
										    </li>
										</a>
									<?php endforeach; ?>
									</ul>
									</section>
								<?php endif; ?>
								<hr class="visible-xs visible-sm">
	                    	</div> <!-- /Col -->

							<div class="col-md-6 col-md-pull-6 week-intro">
								<a class="" title="View <?php echo get_the_title( $weeks->ID );?>" href="<?php echo get_permalink( $weeks->ID ); ?>"><h2><?php echo get_the_title( $weeks->ID ); ?></h2></a>
		                    	<?php the_field( 'weeks_introduction', $weeks->ID ); ?>
								<a class="cta cta-md blue col-sm-6" title="View <?php echo get_the_title( $weeks->ID );?>" href="<?php echo get_permalink( $weeks->ID ); ?>">View <span class="lower"><?php echo get_the_title( $weeks->ID );?></span></a>

		                    </div> <!-- /Col -->

						</div> <!-- /Row -->
					</div> <!-- /Container -->


			<?php } else { ?>
			<?php } ?>

        <?php endforeach; ?>

<?php endif; ?>


<div class="spacer50"></div>
<div class="container">
	<div class="row">
		<div class="col-md-6 forum">
			<h2>Forum</h2>
			<?php bbp_get_template_part( 'bbpress/content', 'archive-topic' ); ?>
		</div> <!-- /Col -->
		<div class="col-md-6">
				<h2>Social feed</h2>
			
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>