<?php 

/*
 * Template Name: FAQ
 * Description: Wellvess faq
 */

 get_header(); ?>
<div class="container sub-page payment">
	<div class="row page-title">
		<div class="col-xs-12">
			<h1><?php the_title(); ?></h1> <?php edit_post_link(); ?></h1>
			<hr>
		</div> <!-- /Col -->
		
	</div> <!-- /Row -->

	<div class="row">
		<section class="content">

			<?php the_content(); ?>	

		</section> <!-- /Col -->
	</div> <!-- /Row -->

	<div class="row">
		<div class="col-md-9">

			<div class="panel-group" id="accordion">

			<?php

			$a = 1;	
			if( have_rows('faq_content') ):

			    while ( have_rows('faq_content') ) : the_row(); ?>
					<?php $item = $a++; ?>
					<div class="panel panel-default faq-panel">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $item ?>">
								<h3><?php the_sub_field('faq_title'); ?></h3>
								<i class="fa fa-plus-circle"></i>
							</a>

						<div id="collapse<?php echo $item ?>" class="panel-collapse collapse faq-body">
							<?php the_sub_field('faq_body'); ?>
						</div>
					</div>

			    <?php endwhile; 

			else :
			endif; ?>

			</div> <!-- /Collapse -->
	
		</div> <!-- /Col -->
		<aside class="col-md-3">

			<?php get_sidebar(); ?>

		</aside> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>