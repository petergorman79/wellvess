<?php
/*
Single Post Template:Recipes
*/

get_header(); ?>



<div class="container">
	
	<div class="row">
		<div class="col-xs-12 col-md-8">

			<div class="row">
				<!-- Recipe Image --> 
				<div class="col-xs-4 visible-xs visible-sm">
					<img src="<?php the_field('recipe_image'); ?>" alt="">
				</div> <!-- /Col -->
				<!-- Recipe time -->
				<div class="col-xs-8 col-md-12">
					<h1><?php echo get_the_title(); ?></h1>
				</div> <!-- /Col -->
				<!-- Recipe prep info -->
				<div class="col-xs-8 col-md-12">
					<ul>
						<li><?php the_field('recipe_serves'); ?></li>
						<li><?php the_field('recipe_prep_time'); ?></li>
						<span style="display:<?php the_field('toggle_cook'); ?>"><li><?php the_field('recipe_cook'); ?></li></span>
					</ul>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

			<hr>
			<!-- Recipe ingredients and untensils --> 
			<div class="row">
				<!-- Ingredients --> 
				<div class="col-xs-12 col-md-6">
					<h3>Ingredients</h3>
					<?php the_field('recipe_ingredients'); ?>
				</div> <!-- /Col -->
				<!-- Utensils --> 
				<div class="col-xs-12 col-md-6">
					<h3>Utensils</h3>
					<?php the_field('recipe_utensils'); ?>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

			<hr>
			<!-- Recipe preparation --> 
			<div class="row"> 
				<div class="col-xs-12">
					<h3>Preparation</h3>
					<?php the_field('recipe_preparation'); ?>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

			<span style="display:<?php the_field('toggle_tips'); ?>">
			<hr>
			<!-- Recipe Cooking tips --> 
			<div class="row" style="display:<?php the_field('toggle_tips'); ?>"> 
				<div class="col-xs-12">
					<h3>Cooking tips</h3>
					<?php the_field('recipe_tips'); ?>
				</div> <!-- /Col -->
			</div> <!-- /Row -->
			</span>
			
			<hr>
			<!-- Recipe Cooking tips --> 
			<div class="row"> 
				<div class="col-xs-12">
					
					<h3>What's good about this recipe</h3>
					<div class="well md c4">
						<?php the_field('recipe_whats_good'); ?>
					</div>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

		</div> <!-- /Col -->

		<div class="col-xs-12 col-md-4">

			<!-- Print --> 
			<div class="row">
				<div class="col-xs-12 print-page">
					<button onclick="print_page()">Print this recipe</button>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

			<!-- Recipe Image --> 
			<div class="row hidden-xs hidden-sm">
				<div class="col-xs-12">
					<img src="<?php the_field('recipe_image'); ?>" alt="">
				</div> <!-- /Col -->
			</div> <!-- /Row -->
			<!-- Recipe Variations --> 
			<span style="display:<?php the_field('toggle_variations'); ?>">
			<div class="row">
				<div class="col-xs-12">
					<h3>Variations</h3>
					<?php the_field('recipe_variations'); ?>
				</div> <!-- /Col -->
			</div> <!-- /Row -->
			</span>
	
		</div> <!-- /Col -->

	</div> <!-- /Row -->
</div> <!-- /Container -->

<?php get_sidebar(); ?>

<script>

	function print_page() {
	    window.print();
	}

</script>

<?php get_footer(); ?>