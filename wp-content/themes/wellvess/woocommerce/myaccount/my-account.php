<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

wc_print_notices(); ?>

<div class="row my-profile">
	<div class="col-xs-12">

			<?php
			printf(
				__( '
					<div class="row account-details">
						<div class="col-xs-12">
							<h2>%1$s %2$s</h2>
						</div> <!-- /Col -->
					</div> <!-- /Row -->


							', 'woocommerce' ) . ' ',
				$current_user->first_name,
				$current_user->last_name,
				$current_user->user_email,
				wc_customer_edit_account_url()
			); ?>

			<form action="" method="post">

				<div class="form-row form-row-first form-group col-md-6">
					<label class="control-label col-xs-12" for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>

					<div class="col-xs-12">
						<input type="text" class="form-control big input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $current_user->first_name ); ?>" />
					</div>
				</div>

				<div class="form-row form-row-first form-group col-md-6">
					<label class="control-label col-xs-12" for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
					<div class="col-xs-12">
						<input type="text" class="form-control big input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $current_user->last_name ); ?>" />
					</div>
				</div>

				<div class="form-row form-row-first form-group col-md-6">
					<label class="control-label col-xs-12" for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
					<div class="col-xs-12">
						<input type="email" class="form-control big input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $current_user->user_email ); ?>" />
					</div>

				</div>

				<div class="spacer10"></div>
				<hr>
				<div class="spacer20"></div>

				<div class="form-row form-row-first form-group col-md-6">
					<label class="control-label col-xs-12" for="password_1"><?php _e( 'Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
					<div class="col-xs-12">
						<input type="password" class="form-control big input-text" name="password_1" id="password_1" />
					</div>
				</div>

				<div class="form-row form-row-first form-group col-md-6">
					<label class="control-label col-xs-12" for="password_2"><?php _e( 'Confirm new password', 'woocommerce' ); ?></label>
					<div class="col-xs-12">
						<input type="password" class="form-control big input-text" name="password_2" id="password_2" />
					</div>
				</div>

				<div class="spacer10"></div>
				<hr>
				<div class="spacer20"></div>

				<div class="form-group col-md-6">
					<input type="submit" class="button cta cta-md green" name="save_account_details" value="<?php _e( 'Save profile changes', 'woocommerce' ); ?>" />
				</div>

				<?php wp_nonce_field( 'save_account_details' ); ?>
				<input type="hidden" name="action" value="save_account_details" />
			</form>

	</div> <!-- /Col -->
</div> <!-- /Row -->

<hr>
<div class="row current-plan">
	<div class="col-xs-12">

		<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>

	</div> <!-- /Col -->
</div> <!-- /Row -->

<?php /*do_action( 'woocommerce_before_my_account' );*/ ?>

<?php /*wc_get_template( 'myaccount/my-downloads.php' );*/ ?>



<?php /*wc_get_template( 'myaccount/my-address.php' );*/ ?>

<?php do_action( 'woocommerce_after_my_account' ); ?>
