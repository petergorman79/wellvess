<?php
/**
 * Lost password form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>



<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
<?php wc_print_notices(); ?>
        <form method="post" class="form">

        	<?php if( 'lost_password' == $args['form'] ) : ?>

                <h1>Forgot your password?</h1>

                <p>Please enter your username or email address below. You will receive an email with a link to create a new password.</p>

                <div class="well lg c4">
                <div class="row">
                    <div class="col-xs-12">
                
                       <div class="form-group">
                            <label for="user_login" class="control-label"><?php _e( 'Username or email', 'woocommerce' ); ?></label>
                            <input class="form-control big" type="text" name="user_login" id="user_login" placeholder="Enter your usernaeme or email address" />
                        </div> 
                
                    </div> <!-- /Col -->
                </div> <!-- /Row -->

        	<?php else : ?>

                <p><?php echo apply_filters( 'woocommerce_reset_password_message', __( 'Enter a new password below.', 'woocommerce') ); ?></p>


                <div class="row">
                    <div class="col-xs-12">
                
                        <div class="form-group">
                            <label for="password_1" class="control-label"><?php _e( 'New password', 'woocommerce' ); ?> <span class="required">*</span></label>
                            <input type="password" class="form-control big" name="password_1" id="password_1" placeholder="Enter your new password" />
                        </div>

                        <div class="form-group">
                            <label for="password_2" class="control-label"><?php _e( 'Re-enter new password', 'woocommerce' ); ?> <span class="required">*</span></label>
                            <input type="password" class="form-control big" name="password_2" id="password_2" placeholder="Re-enter your new password" />
                        </div>

                        <input type="hidden" name="reset_key" value="<?php echo isset( $args['key'] ) ? $args['key'] : ''; ?>" />
                        <input type="hidden" name="reset_login" value="<?php echo isset( $args['login'] ) ? $args['login'] : ''; ?>" />
                
                    </div> <!-- /Col -->
                </div> <!-- /Row -->

            <?php endif; ?>

            <div class="clear"></div>

            <div class="row">
                <div class="col-xs-12">

                    <p class="form-row"><input type="submit" class="cta cta-sm blue col-xs-12 col-sm-6 col-md-5" name="wc_reset_password" value="<?php echo 'lost_password' == $args['form'] ? __( 'Reset password', 'woocommerce' ) : __( 'Reset password', 'woocommerce' ); ?>" /></p>
                	<?php wp_nonce_field( $args['form'] ); ?>

                </div> <!-- /Col -->
            </div> <!-- /Row -->
            </div> <!-- /Well --> 

        </form>

        </div> <!-- /Col -->
    </div> <!-- /Row -->
</div> <!-- /Container -->