<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<?php endif; ?>

<div class="container">
	<div class="row ">
		<div class="col-xs-12">

			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-5 col-md-offset-0">
			
					<h1><?php _e( 'Login', 'woocommerce' ); ?></h1>

					<div class="well sm c4">
					<form method="post" role="form" class="form">

						<?php do_action( 'woocommerce_login_form_start' ); ?>
						
						<div class="form-group">
							<label for="username" class="control-label"><?php _e( 'Username or email address', 'woocommerce' ); ?></label>
							<input type="text" class="form-control big" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
						</div>
						<div class="form-group">
							<label for="password" class="control-label"><?php _e( 'Password', 'woocommerce' ); ?></label>
							<input class="form-control big" type="password" name="password" id="password" />
						</div>

						<?php do_action( 'woocommerce_login_form' ); ?>

						<div class="form-group">
							
							<?php wp_nonce_field( 'woocommerce-login' ); ?>
							<input type="submit" class="cta cta-sm blue col-xs-12 col-md-4" name="login" value="<?php _e( 'Login', 'woocommerce' ); ?>" /> 
							<label for="rememberme" class="col-xs-12 col-md-6">
								<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
							</label>
						</div>
						<div class="spacer15"></div>
						<a href="<?php echo esc_url( wc_lostpassword_url() ); ?>"><?php _e( 'I forgot my password', 'woocommerce' ); ?></a>

						<input type="hidden" name="redirect" value="<?php echo site_url();?>/my-program/" />

						<?php do_action( 'woocommerce_login_form_end' ); ?>

						<div class="clearfix"></div>
					</form>
					</div>
			
				</div> <!-- /Col -->

				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-1">
			
					<h1>Join Wellvess today</h1>
					<p>Here's what you get from the 8 week program</p>
					<ul class="body">
						<li>Dietician approved meal-plans for 8 weeks</li>
						<li>Weekly shopping lists to print</li>
						<li>How to cook with anti-inflammatory ingredients</li>
						<li>Tips and help with exercise</li>
						<li>Weekly in-depth features on how to manage arthritis</li>
					</ul>
					<a href="<?php echo site_url(); ?>/join-wellvess/" class="cta cta-lg green col-xs-12 col-sm-7 col-md-5">Join now!</a>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container -->




<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div class="col-2">

		<h2><?php _e( 'Register', 'woocommerce' ); ?></h2>

		<form method="post" class="register">

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="form-row form-row-wide">
					<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
				</p>

			<?php endif; ?>

			<p class="form-row form-row-wide">
				<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
			</p>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
	
				<p class="form-row form-row-wide">
					<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="password" class="input-text" name="password" id="reg_password" />
				</p>

			<?php endif; ?>

			<!-- Spam Trap -->
			<div style="left:-999em; position:absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>

			<p class="form-row">
				<?php wp_nonce_field( 'woocommerce-register', 'register' ); ?>
				<input type="submit" class="button" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
			</p>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
