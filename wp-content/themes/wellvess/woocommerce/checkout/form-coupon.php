<?php
/**
 * Checkout coupon form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

if ( ! WC()->cart->coupons_enabled() ) {
	return;
}

wc_print_notice( $info_message, 'notice' );
?>

	<div class="row">
		<div class="col-md-4">
			<h3>Promotional code</h3>
		</div> <!-- /Col -->
			
			<form method="post">

				<div class="col-md-4">
					<input type="text" name="coupon_code" class="form-control big" id="coupon_code" value="" />
				</div>

				<div class="col-sm-6 col-md-4 apply-code">
					<input type="submit" class="cta cta-sm blue" name="apply_coupon" value="<?php _e( 'Apply code', 'woocommerce' ); ?>" />
				</div>

				<div class="clear"></div>
			</form>

	</div> <!-- /Row -->
