<?php
/**
 * Show error messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! $messages ) return;
?>

	<div class="row">
		<div class="col-xs-12">

			<div class="alert alert-danger" role="alert">
				<?php foreach ( $messages as $message ) : ?>
				<div class="row">
					<div class="col-xs-12">
						<?php echo wp_kses_post( $message ); ?>
					</div> <!-- /Col -->
				</div> <!-- /Row -->
				<?php endforeach; ?>
			</div>

		</div> <!-- /Col -->
	</div> <!-- /Row -->