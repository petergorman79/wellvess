<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>

	
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

		<li class="product-item">
			<h3><?php the_title(); ?></h3>

			<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );
			?>

			<?php
			global $post;
			$terms = wp_get_post_terms( $post->ID, 'product_cat' );
			foreach ( $terms as $term ) $categories[] = $term->slug;

			if ( in_array( 'annual', $categories ) ) { ?>
	  			 <p> <?php echo 'Best value, save $35'; ?> </p>
			<?php } elseif ( in_array( 'weekly', $categories ) ) { ?>
	  			<p> <?php echo 'Total = $200'; ?> </p>
			<?php } ?>

			<?php do_action( 'wellvess_join_button' ); ?>
		</li>