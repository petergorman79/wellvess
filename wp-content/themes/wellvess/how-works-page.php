<?php 

/*
 * Template Name: How it works
 */

 get_header(); ?>

<div class="wrapper c1 sub">
<div class="container sub-page payment">
	<div class="row page-title">
		<div class="col-xs-12">
			
		

		<div class="row">
				<div class="col-md-6">
					<h1><?php the_title(); ?></h1> <?php edit_post_link(); ?></h1>
					<p>In this program, we walk you step by step through a holistic approach to arthritis care. Using the latest, scientific research and health practitioner advice, we have created a one-stop arthritis resource that offers tips, advice and guidance on living well with arthritis.
Of course, the Wellvess program is not intended to replace the medical advice given to you by your general practitioner or specialist.</p>
			
				</div> <!-- /Col -->
				<div class="col-md-6">
			
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="//www.youtube.com/embed/gC2ze6BpZIQ"></iframe>
					</div>
				</div> <!-- /Col -->

			</div> <!-- /Row -->
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 
</div>
<div class="container sub-page payment">
	<div class="row">
		<section class="col-md-9 content">

			<?php the_content(); ?>	

		</section> <!-- /Col -->
		<aside class="col-md-3">

			<?php get_sidebar(); ?>

		</aside> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>