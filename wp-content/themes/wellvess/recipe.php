<?php  get_header(); ?>
<div class="container sub-page payment">
	<div class="row">
		<div class="col-xs-12">
			<div class="row page-title">
				<div class="col-xs-12">
					<h1><?php the_title(); ?></h1> <?php edit_post_link(); ?></h1>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

			<hr>

			<?php the_content(); ?>	

		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>