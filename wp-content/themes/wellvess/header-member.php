<div class="wrapper program">

<div class="container program-header">
	<?php $current_user = wp_get_current_user(); ?>

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-7">
			<a href="<?php echo site_url(); ?>/my-program/"><h1><?php echo $current_user->user_firstname; ?> <?php echo $current_user->user_lastname; ?></h1></a>

			<?php if ( is_page('my-program') ) { // Displays program title instead of week (as below) ?>

			<?php } else { ?>

			<?php 
				$category = get_the_category();
				$firstCategory = $category[0]->cat_name;
			?>	
			<?php if ( $firstCategory == 'Preparation' ) { ?>
			<h3><?php echo get_the_title(); ?>aration week</h3>
			<?php } else { ?>
			<h3><?php echo get_the_title(); ?></h3>
			<?php } ?>

			<?php } ?> 
	
		</div> <!-- /Col -->
		<div class="col-xs-12 col-sm-6 col-md-5">

			<?php $my_date = date( 'l j M, Y', current_time( 'timestamp', 0 ) ); ?>
			<h2><?php echo $my_date ?></h2>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container -->

</div> <!-- /Wrapper --> 