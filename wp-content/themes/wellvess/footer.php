<div class="clearfix"></div>

<div class="spacer50"></div>

<?php if (is_front_page() ) { ?>

<?php } else { ?>
<div class="wrapper top c1">
<?php } ?>
<footer class="container">
	<div class="row">
		<div class="col-xs-12">
			<ul>
				<?php wp_footer_menu_no_ul( array( 'theme_location' => 'footer-menu' ) ); ?>
			</ul>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
	<div class="row">
		<div class="col-xs-12">
			<?php if (is_front_page() ) { ?>
			<img class="wv-logo" src="<?php bloginfo('template_directory'); ?>/img/wellvess.svg" alt="Wellvess - Arthritis Vitality Program" />
			<?php } else { ?>
			<img class="wv-logo" src="<?php bloginfo('template_directory'); ?>/img/wellvess_footer.svg" alt="Wellvess - Arthritis Vitality Program" />
			<?php } ?>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
	<div class="row">
		<div class="col-xs-12">
			<span><?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); ?></span>
			<ul>
				<?php wp_terms_menu_no_ul( array( 'theme_location' => 'terms-menu' ) ); ?>
			</ul>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</footer> <!-- /Container -->
<?php if (is_front_page() ) { ?>

<?php } else { ?>
</div>
<?php } ?>

<!-- Disclaimer -->
<?php if (is_front_page() ) { ?>
<div class="wrapper c7">
<?php } else { ?>
<div class="wrapper dark c7">
<?php } ?>

<section class="container section-7">
	<div class="row">
		<div class="col-xs-12 disclaimer">
			<p>
				This website contains general information about medical conditions and treatments, diet, recipes, lifestyle and exercise.  Any information provided is not advice, and should not be treated as such. You should never delay seeking medical advice, disregard medical advice, or discontinue or change medical treatment because of information on this website, unless advised by your doctor. <a href="<?php echo site_url(); ?>/medical-disclaimer/" >Read full disclaimer.</a> 
			</p>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
</div> <!-- /Wrapper -->
<!-- /Disclaimer -->

<!--  Google Analytics --> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54127007-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Js -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/plugins/svgeezy-master/svgeezy.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/plugins/stellar.js-master/jquery.stellar.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/plugins/jQuery.mmenu-master/src/js/jquery.mmenu.min.all.js"></script>

<script>
$(document).ready(function() {
	$("#my-menu").mmenu({
		"offCanvas": {
          "zposition": "next"
    	},
	});
	$("#my-button").click(function() {
		$("#my-menu").trigger("open.mm");
	});
});

$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

$('.member-account-menu').mouseenter(function(){
  $(this).find('.member-sub-menu').slideDown('fast');
});
$('.member-account-menu').mouseleave(function(){
  $(this).find('.member-sub-menu').slideUp('fast');
});

$('#menu-item-103').mouseenter(function(){
  $(this).find('.sub-menu').slideDown('fast');
});
$('#menu-item-103').mouseleave(function(){
  $(this).find('.sub-menu').slideUp('fast');
});

$('#menu-item-104').mouseenter(function(){
  $(this).find('.sub-menu').slideDown('fast');
});
$('#menu-item-104').mouseleave(function(){
  $(this).find('.sub-menu').slideUp('fast');
});

$('#menu-item-105').mouseenter(function(){
  $(this).find('.sub-menu').slideDown('fast');
});
$('#menu-item-105').mouseleave(function(){
  $(this).find('.sub-menu').slideUp('fast');
});

$('#menu-item-106').mouseenter(function(){
  $(this).find('.sub-menu').slideDown('fast');
});
$('#menu-item-106').mouseleave(function(){
  $(this).find('.sub-menu').slideUp('fast');
});

$('#menu-item-107').mouseenter(function(){
  $(this).find('.sub-menu').slideDown('fast');
});
$('#menu-item-107').mouseleave(function(){
  $(this).find('.sub-menu').slideUp('fast');
});

$('.program-weeks-mobile ul').hide(); //Hide children by default
	
	$('#current_week').click(function(){
	event.preventDefault();
	$(this).siblings('.program-weeks-mobile ul').slideToggle('fast');
	});

svgeezy.init(false, 'png');

</script>

</div>

<?php wp_footer(); ?>
</body>
</html>