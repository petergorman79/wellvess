<?php
/*
Template Name: Active Programs
*/

get_header(); ?>

<?php get_header( 'member' ); ?>

<?php get_header( 'program_weeks' ); ?>

<?php $active_program = get_field( 'active_program' ); // Gets the active program ?>  
    <?php if( $active_program ): ?>

		<?php foreach( $active_program as $active_program ): // Sets loop ?> 

			<?php if ( get_post_status ( $active_program->ID ) == 'publish' ) {  // Checks the post status - if published display below ?>

					<?php $weeks = get_field( 'program_weeks', $active_program->ID ); // Gets the weeks from the active program --> ?> 
					    <?php if( $weeks ): ?>

							<?php foreach( $weeks as $weeks ): ?>

								<?php if ( get_post_status ( $weeks->ID ) == 'publish' ) { // Checks the post status - if published display below ?>

								<div class="container">
									<div class="row">
										<?php 
											$category = get_the_category( $weeks->ID );
											$firstCategory = $category[0]->cat_name;
										?>

										<div class="col-md-6 week-intro">
											<a class="" title="View <?php echo get_the_title( $weeks->ID );?>" href="<?php echo get_permalink( $weeks->ID ); ?>">
												<?php if ( $firstCategory == 'Preparation' ) { ?>
												<h1><?php echo get_the_title( $weeks->ID ); ?>aration week</h1> <i class="fa fa-long-arrow-right"></i>
												<?php } else { ?>
												<h1><?php echo get_the_title( $weeks->ID ); ?></h1>
												<?php } ?>
											</a>
					                    	<?php the_field( 'weeks_introduction', $weeks->ID ); ?>
					                    	<div class="spacer10"></div>
											<a class="cta cta-md blue col-sm-6" title="View <?php echo get_the_title( $weeks->ID );?>" href="<?php echo get_permalink( $weeks->ID ); ?>">View 
												<?php if ( $firstCategory == 'Preparation' ) { ?>
												<span><?php echo get_the_title( $weeks->ID ); ?> week</span>
												<?php } else { ?>
												<span><?php echo get_the_title( $weeks->ID ); ?></span>
												<?php } ?></a>
											<div class="spacer10"></div>
					                    </div> <!-- /Col -->

										
										<?php if ( $firstCategory == 'Preparation' ) { ?>
										
										<div class="col-md-6 welcome-message">
										<div class="well md c4">
					                    	<h2>Welcome and congratulations on signing up for the Wellvess Vitality Program!</h2>
					                    	<p>This program is a brilliant way to take charge of your arthritis. It’s easy and affordable, and most of all, it works.</p>

					                    	<p> Over the coming weeks we’ll be sharing loads of exciting material with you to help you stay on top of your arthritis and pain management goals. We’ve got meal plans, recipes, shopping lists and lots of wonderful insights and strategies to share.</p>

					                    	<p>We hope you love this program as much as we do!</p>

					                    	<p><em><br>The Wellvess Team</em></p>
					                    <div class="spacer20"></div>
					                    </div>
					                    </div> <!-- /Col -->
										
										<?php } else { ?>

										<div class="col-md-6">
					                    	<?php
											$append = week_;
											$my_date = date( 'l', current_time( 'timestamp', 0 ) );
											$archive_day = $append.$my_date //Creates day of the week for relationship ?> 

											<?php $recipes = get_field( $archive_day , $weeks->ID ); //Gets the recipe for the current day ?>
											
											<?php if( $recipes ):
											$category = get_the_category();
											$firstCategory = $category[0]->cat_name; ?>
												<section class="food-tile">
													<h2>Todays meal plan</h2>
												<ul>
												<?php foreach( $recipes as $r ): ?>
												    <a href="<?php echo get_permalink( $r->ID ); ?>">
												    	<li>
												    		<img src="<?php the_field('recipe_image', $r->ID); ?>" alt="">
												    		<h5><?php echo get_the_title( $r->ID ); ?></h5>
													    </li>
													</a>
												<?php endforeach; ?>
												</ul>
												</section>
											<?php endif; ?>
											<hr class="visible-xs visible-sm">
				                    	</div> <!-- /Col -->
					                    <?php } ?>

									</div> <!-- /Row -->
								</div> <!-- /Container -->

									<?php } else { ?>
								<?php } ?>

					        <?php endforeach; ?>

					<?php endif; ?>

			<?php } else { ?>
			<?php } ?>

        <?php endforeach; ?>

<?php endif; ?>
<div class="spacer20"></div>

<div class="wrapper">
<div class="container">
	<div class="row">
		<hr>
		<div class="col-md-6 forum">
			<h2>Forum</h2>
			<?php bbp_get_template_part( 'bbpress/content', 'archive-topic' ); ?>
			<a class="cta cta-md blue col-sm-6" title="Join the conversation" href="<?php echo site_url(); ?>/forums/">Join the conversation</a> 
		</div> <!-- /Col -->
		<div class="col-md-6">
				<h2>Social feed</h2>
			
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container -->
</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>