

/* xs */
@media (max-width: 768px) {

	.hero {
		background: url(<?php the_field('hero_image_xs');?>);
	}

}

/* sm */
@media (min-width: 768px) {

	.hero {
		background: url(<?php the_field('hero_image_sm');?>);
	}

}

/* md */
@media (min-width: 992px) {

	.hero {
		background: url(<?php the_field('hero_image_lg');?>);
	}

}

/* lg */
@media (min-width: 1200px) {

	.hero {
		background: url(<?php the_field('hero_image_lg');?>);
	}

}