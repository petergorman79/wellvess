<article class="container sub-page" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row page-title">
		<div class="col-xs-12">
			<?php if ( is_singular() ) {
				echo '<h1 class="entry-title">';
			} else {
				echo '<h1 class="entry-title">';
			}
			?>

			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
			<?php if ( is_singular() ) {
				echo '</h1>';
			} else { 
				echo '</h2>';
			}
			?>
			<hr>
		</div> <!-- /Col -->
		
	</div> <!-- /Row -->
	<div class="row">
		<div class="col-md-9">
 			<!--<div>
				<?php edit_post_link(); ?>
				<?php if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
			</div> -->

				<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
				<?php if ( !is_search() ) get_template_part( 'entry-footer' ); ?>
		</div> <!-- /Col -->
		<aside class="col-md-3">
			<?php get_sidebar(); ?>
		</aside> <!-- /Col -->
		<div class="col-md-9">
			<?php get_template_part( 'nav', 'below-single' ); ?>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</article>