<div class="wrapper weeks">

<div class="container">
	<div class="row">
		<div class="col-xs-12">

			<?php $active_program = get_field( 'active_program', 518 ); // Gets the active program ?>  
		    <?php if( $active_program ): ?>

				<?php foreach( $active_program as $active_program ): // Sets loop ?> 

					<?php if ( get_post_status ( $active_program->ID ) == 'publish' ) {  // Checks the post status - if published display below ?>

					<?php
					// Sets up the weeks nav
					$posts = get_field( 'program_weeks', $active_program->ID );
					if( $posts ): ?>

						<nav class="program-weeks hidden-xs">
							<ul>
							<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
								
								<?php if ( get_post_status ( $p->ID ) == 'future' ) { ?>
								<li class="future"> <?php echo get_the_title( $p->ID );?></li>
								<?php } elseif ( get_post_status ( $p->ID ) == 'publish' ) { ?>
								<li class="publish"><a href="<?php echo get_permalink( $p->ID ); ?>"><i class="fa fa-child hidden-xs hidden-sm"></i> <?php echo get_the_title( $p->ID );?></a></li>
								<?php } else { ?>
								<li class="draft"><a href="<?php echo get_permalink( $p->ID ); ?>"><i class="fa fa-check hidden-xs hidden-sm"></i> <?php echo get_the_title( $p->ID );?></a></li>
								<?php } ?>
							    
							<?php endforeach; ?>
							</ul>
						</nav>

						<nav class="program-weeks-mobile visible-xs">
							<div class="spacer20"></div>
							<?php foreach( $posts as $current_week ): // variable must NOT be called $post (IMPORTANT) ?>

								<?php if ( get_post_status ( $current_week->ID ) == 'publish' ) { ?>
									<a id="current_week" class="cta cta-md blue"><?php echo get_the_title( $current_week->ID );?></a>
								<?php } else { ?>
								 
								<?php } ?>

							<?php endforeach; ?>
							<ul>
							<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

							
								<?php if ( get_post_status ( $p->ID ) == 'future' ) { ?>
								<li class="future"> <?php echo get_the_title( $p->ID );?></li>

								<?php } elseif ( get_post_status ( $p->ID ) == 'publish' ) { ?>
								<li class="publish"><a href="<?php echo get_permalink( $p->ID ); ?>"><i class="fa fa-child"></i> <?php echo get_the_title( $p->ID );?></a></li>

								<?php } else { ?>
								<li class="draft"><a href="<?php echo get_permalink( $p->ID ); ?>"><i class="fa fa-check"></i> <?php echo get_the_title( $p->ID );?></a></li>
								<?php } ?>
							

							<?php endforeach; ?>
							</ul>  
							<div class="spacer20"></div>
						</nav>

					<?php endif; ?>

					<?php } else { ?>
					<?php } ?>

        		<?php endforeach; ?>

			<?php endif; ?>

		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container -->

</div> <!-- /Wrapper --> 