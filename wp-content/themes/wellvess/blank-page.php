<?php 

/*
 * Template Name: Blank Page
 * Description: This is a blank template
 */

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-xs-12">

			<?php the_content(); ?>	

		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container -->


<?php get_sidebar(); ?>

<?php get_footer(); ?>