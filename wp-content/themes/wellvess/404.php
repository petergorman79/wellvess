<?php get_header(); ?>
<div class="container sub-page">
	<div class="row page-title">
		<div class="col-xs-12">
			<h1>Ooops...</h1>
			<hr>
		</div> <!-- /Col -->
		
	</div> <!-- /Row -->

	<div class="row">
		<section class="col-md-9 content">

			<p>Something has gone wrong. We could'nt find the page you are after.</p>
			<a  href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( get_bloginfo( 'name' ) ); ?>" class="cta cta-md blue col-xs-12 col-sm-6" rel="home">Return home</a>

		</section> <!-- /Col -->
		<!-- <aside class="col-md-3">
		 
		 	<?php get_sidebar(); ?>
		 
		 </aside> --> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>