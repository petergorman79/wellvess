<?php 

/*
 * Template Name: Checkout
 * Description: Template for the checkout page
 */

 get_header(); ?>
<div class="container sub-page payment">
	<div class="row">
		<div class="col-xs-12">
			<div class="row page-title">
				<div class="col-xs-12 col-sm-11">
					<h1><?php the_title(); ?></h1> <?php edit_post_link(); ?></h1>
				</div> <!-- /Col -->
				<div class="col-sm-1 hidden-xs">
					<div class="secure-page">
						<img src="<?php bloginfo('template_directory'); ?>/img/lock.svg" alt="">
					</div>
				</div> <!-- /Col -->
			</div> <!-- /Row -->

			<hr>

			<div class="row">
				<div class="col-xs-12 col-md-8">
					<?php the_content(); ?>	
				</div> <!-- /Col -->

				<div class="col-xs-12 col-md-4">
					<?php get_sidebar(); ?>	
				</div> <!-- /Col -->
			</div> <!-- /Row -->

		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container --> 

<?php get_footer(); ?>