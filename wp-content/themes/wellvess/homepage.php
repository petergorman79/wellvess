<?php 

/*
 * Template Name: Homepage
 * Description: Wellvess homepage
 */

 get_header(); ?>

<div class="hero">

	<div class="container hero-message">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-7">

			  <h1>
			  	<?php the_field('hero_title');?>
			  </h1>
			  <h3 style="display:<?php the_field('toggle_hero_message');?>">
			  	<?php the_field('hero_message');?>
			  </h3>
				
				<?php if ( is_user_logged_in() ) {
				$current_user = wp_get_current_user(); ?>
					<a href="<?php echo site_url(); ?>/my-program/" class="cta cta-lg green col-xs-12 col-sm-9">Hi <?php echo $current_user->user_firstname; ?>, get started!</a>
				<?php } else { ?>
					<a href="<?php echo site_url(); ?>/join-wellvess/" class="cta cta-lg green col-xs-12 col-sm-7">Join today</a>
				<?php } ?>

  			</div> <!-- /Col -->
		</div> <!-- /Row -->
	</div> <!-- /Container -->

	<div class="next-program" style="display:<?php the_field('toggle_program_panel');?>">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
						<h4>Next program starts 6 October 2014</h4>
				</div> <!-- /Col -->
			</div> <!-- /Row -->
		</div> <!-- /Container -->
	</div>
<div class="cleafix"></div>
</div> <!-- /Hero -->



<!-- Homepage sections --> 
<?php if( have_rows('homepage_sections') ): ?>
    <?php while ( have_rows('homepage_sections') ) : the_row(); ?>

        <?php if( get_row_layout() == 'introduction' ): ?>
			
			<!-- Intro Sections --> 
        	<?php if( have_rows('intro_sections') ): ?>

			    <?php while ( have_rows('intro_sections') ) : the_row(); ?>
					
					<!-- Blue -->
			        <?php if( get_row_layout() == 'intro_blue' ): ?>        	
					<div class="wrapper c1"> 
					<section class="container section-1 top">
						<div class="row">
							<div class="col-xs-12">
								<div class="img">
									<img src="<?php the_sub_field('image'); ?>" />
								</div>
								<div class="content">
									<div class="v-wrap">
										<div class="v-inner">
											<h2><?php the_sub_field('intro_title'); ?></h2>
											<p><?php the_sub_field('intro_message'); ?></p>
										</div>
									</div>
								</div>
							</div> <!-- /Col -->
						</div> <!-- /Row -->
					</section> <!-- /Container -->
					</div> <!-- /Wrapper -->
			        <?php endif; ?>
			        <!-- /Blue -->
					
					<!-- Grey -->
			        <?php if( get_row_layout() == 'intro_grey' ): ?>

					<div class="wrapper c4"> 
					<section class="container section-1 bottom">
						<div class="row">
							<div class="col-xs-12">
								<div class="img">
									<img src="<?php the_sub_field('image'); ?>" />
								</div>
								<div class="content">
									<div class="v-wrap">
										<div class="v-inner">
											<h2><?php the_sub_field('intro_title'); ?></h2>
											<p><?php the_sub_field('intro_message'); ?></p>
										</div>
									</div>
								</div>
							</div> <!-- /Col -->
						</div> <!-- /Row -->
					</section>
					</div> <!-- /Wrapper -->
        			<?php endif; ?>
        			<!-- /Grey -->

   				<?php endwhile; ?>

			<?php else : ?>
			<?php endif; ?>
			<!-- /Intro Sections -->

        <?php endif; ?>

    <?php endwhile; ?> <!-- /Introduction -->

<?php else : ?>
<?php endif; ?> <!-- /Homepage sections --> 


<!-- Section 2 - How it works -->
<section class="container section-2">
	<div class="row">
		<div class="col-xs-12">
			<h2>How the program works</h2>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
	<div class="row">
		<div class="col-md-6">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="//www.youtube.com/embed/gC2ze6BpZIQ?rel=0"></iframe>
			</div>
		</div> <!-- /Col -->
		<div class="col-md-6">
			<p>
				In this program, we walk you step by step through a holistic approach to arthritis care. Using the latest, scientific research and health practitioner advice, we have created a one-stop arthritis resource that offers tips, advice and guidance on living well with arthritis.
			</p>

			<p>
				Of course, the Wellvess program is not intended to replace the medical advice given to you by your general practitioner or specialist. 
			</p>

			<?php if ( is_user_logged_in() ) { ?>
				
			<?php } else { ?>
				<a href="<?php echo site_url(); ?>/join-wellvess/" class="cta cta-lg green col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 col-md-7 col-md-offset-0">Join today</a>
			<?php } ?>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
<!-- /Section 2 -->

<!-- Section 3 - Who is it for -->
<div class="wrapper section-3-img">
			<div class="img">&nbsp;</div>
</div> <!-- /Wrapper -->

<div class="wrapper c4">
<section class="container section-3">
	<div class="row">
		<div class="col-xs-12">
			<h2>Who is Wellvess for?</h2>
			<p>
				The Wellvess Arthritis Vitality Program is designed for people who are currently living with the debilitating side effects of arthritis.
			</p>
			<p>
				If you answer ‘yes’ to more than one of the following questions, then this program could be for you:
			</p>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
	<div class="row">
		<div class="col-xs-12">
			<div class="multiColumn">
				<ul>
					<li>Do you feel restricted by your arthritis?</li>
					<li>Are you in pain and unhappy?</li>
					<li>Do you feel that you are missing out on activities with
family and friends?</li>
					<li>Is arthritis affecting how you work or socialise?</li>
					<li>Do you wish to be in control of your health?</li>
					<li>Are you relying solely on prescription drugs to treat your arthritis?</li>
				</ul>
			</div>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
</div> <!-- /Wrapper -->
<!-- /Section 3 -->

<!-- Section 4 - What you get -->
<section class="container section-4">
	<div class="row">
		<div class="col-xs-12">
			<h2>What will you get from this program?</h2>
			<p>
				For less than $3 per day, the Wellvess Arthritis Vitality Program will equip you with the information and support you need to get your life back on track.
			</p>
			<p>
				Aside from providing you with loads of useful information on your condition and it's management, the 8 week program also includes:
			</p>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
	<div class="row">
		<div class="col-xs-12">
			
			<div class="row">
				<div class="col-xs-6 col-md-3">
					<div class="thumbnail">
						<div class="dietician"><img src="<?php bloginfo('template_directory'); ?>/img/approved.svg" /></div>
						<img src="<?php bloginfo('template_directory'); ?>/img/8weeks.svg" />
						<div class="caption">
					        <p>
					        	Meal plans & recipes
							</p>
					    </div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 ">
					<div class="thumbnail">
						<img src="<?php bloginfo('template_directory'); ?>/img/shopping.svg" />
						<div class="caption">
					        <p>
					        	Shopping lists & cooking tips
							</p>
					    </div>
				    </div>
				</div>
				<div class="col-xs-6 col-md-3">
					<div class="thumbnail">
						<img src="<?php bloginfo('template_directory'); ?>/img/goals.svg" />
						<div class="caption">
					        <p>
					        	Tips on goal setting
							</p>
					    </div>
				    </div>
				</div>
				<div class="col-xs-6 col-md-3">
					<div class="thumbnail">
						<img src="<?php bloginfo('template_directory'); ?>/img/topics.svg" />
						<div class="caption">
					        <p>
					        	Weekly special topics
							</p>
					    </div>
				    </div>
				</div>
			</div> <!-- /Row -->

		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
<!-- /Section 4 -->
<!-- Section 5 - Products -->
<?php if ( is_user_logged_in() ) { ?>
	<div class="spacer30"></div>
<?php } else { ?>
<section class="container section-5">
	<div class="row">
		<div class="col-xs-12">
			<h2>Are you ready to take control of your arthritis? Join today! </h2>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
	<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
			<div class="products">
				<?php echo do_shortcode( '[recent_products per_page="2" order="asc"]' ) ?>
			</div>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
<?php } ?>
<!-- /Section 5 -->

<!-- Section 6 - Testimonial -->
<div class="wrapper c4 b-top b-bottom">
<section class="container section-6">
	<div class="row">
		
		<div class="col-xs-12 col-sm-9 col-sm-push-3 col-md-8">
			<p>
				"The meal plans have been designed to help reduce pain and inflammation through eating nutritious foods that have anti-inflammatory properties. The recipes are easy to source and prepare, low in kilojoules and most importantly are tasty!"
			</p>
				<small><strong>Shivaun Conn</strong>, Accredited Practising Dietitian (APD) and Accredited Nutritionist (AN), sub-committee member of the Australian Lifestyle Medicine Association (ALMA)</small>
		</div> <!-- /Col -->
		<div class="col-xs-8 col-xs-offset-2 col-sm-3 col-sm-pull-9 col-sm-offset-0 col-md-offset-0">
			<img src="<?php bloginfo('template_directory'); ?>/img/team_Shivaun_x2.png" />
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
</div> <!-- /Wrapper -->
<!-- /Section 6 -->

<!-- Social / Newsletter -->
<div class="wrapper c1">
<section class="container social">
	<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0 col-md-push-6">
			<h2>Join our mailing list</h2>

				
				<div class="form">
					<div class="col-sm-12">
						<p>Sign up below to receive the Wellvess newsletter</p>
					</div>

					<?php if( function_exists( 'mc4wp_form' ) ) { mc4wp_form(); } ?>
					
				</div>
		</div> <!-- /Col -->
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0 col-md-pull-6">
			<h2>Connect with Wellvess</h2>

			<ul>
				<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/fb.svg" /></a></li>
				<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/twit.svg" /></a></li>
				<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/tube.svg" /></a></li>
			</ul>

		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
</div> <!-- /Wrapper -->

<!-- /Social / Newsletter -->

<div class="wrapper c4 b-top b-bottom">
<section class="container like-box">
	<div class="row">
		<div class="col-xs-12">
			<!-- <div id="container" style="width:100%;">
				<div  class="fb-like-box"  data-href="https://www.facebook.com/news.com.au" data-width="292" data-height="180" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
			</div> -->
		</div>
	</div> <!-- /Row -->
</section> <!-- /Container -->
</div> <!-- /Wrapper -->

<?php get_footer(); ?>