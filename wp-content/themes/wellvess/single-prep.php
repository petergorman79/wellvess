<?php
/*
Single Post Template:Prep week
*/

get_header(); ?>

<?php get_header( 'member' ); ?>

<?php get_header( 'program_weeks' ); ?>
<div class="spacer20"></div>
<div class="container">

	<div class="row hidden-xs">
		<div class="col-xs-12 col-sm-6 col-md-7">
			<div class="week-breadcrumb">
			<a href="<?php echo site_url(); ?>/my-program/"><i class="fa fa-home"></i></a> <span>/ <?php echo get_the_title(); ?></span>
			</div>
		</div> <!-- /Col -->
	</div> <!-- /Row  -->

	
	<div class="row">
		<div class="col-md-12">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs wv-tabs" role="tablist">
			<li class="active"><a href="#intro" role="tab" data-toggle="tab"><h4>Get ready</h4></a></li>
			<li><a href="#topics" role="tab" data-toggle="tab"><h4>Key topics</h4></a></li>
			<li><a href="#shopping" role="tab" data-toggle="tab"><h4>Shopping list</h4></a></li>
			
		</ul>

		</div> <!-- /Col -->
		<div class="col-md-12">

		<!-- Tab panes -->
		<div class="tab-content">
			<!-- <div class="tab-pane fade in active" id="intro">
				
				<div class="row">
					<div class="col-xs-12">
						<?php the_field('weeks_introduction');?>
					</div> /Col
				</div> /Row
			
			</div> -->

			<div class="tab-pane fade in active" id="topics">
				
				<div class="row">
					<div class="col-xs-12">
						<?php the_field('weeks_topics');?>
					</div> <!-- /Col -->
				</div> <!-- /Row -->

			</div>

			<div class="tab-pane fade" id="shopping">
				<div class="row">
					<div class="col-xs-12">
						<?php the_field('weeks_shopping');?>
					</div> <!-- /Col -->
				</div> <!-- /Row -->
			</div>

			<div class="tab-pane fade" id="meals">

			<div class="row">
				<div class="col-xs-12 col-sm-12">

					<div class="row">

						<div class="col-xs-12 weeks-meal-plan">
						<?php 
						$meals = get_field('week_Sunday');
						if( $meals ): ?>
							<h3>Sunday</h3>
							<ul>
							<?php foreach( $meals as $meals ): // variable must NOT be called $post (IMPORTANT) ?>
							    <li>
							    	<a href="<?php echo get_permalink( $meals->ID ); ?>">
							    		<article>
							    			<?php $category = get_the_category( $meals->ID ); ?> 
											<h3><?php echo $category[0]->cat_name; ?></h3>
							    			<img src="<?php the_field('recipe_image', $meals->ID); ?>" />
							    			<div class="recipe-details">
							    				<span><i class="fa fa-user"></i> <?php the_field('recipe_serves', $meals->ID); ?></span>
							    				<i class="fa fa-clock-o"></i> <?php the_field('recipe_prep_time', $meals->ID); ?>
							    			</div>
							    			<p><?php echo get_the_title( $meals->ID ); ?></p>
							    		</article>
							    		<a href="<?php echo get_permalink( $meals->ID ); ?>" class="cta cta-sm green">Preview</a>
							    	</a>
							    </li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>	
						</div> <!-- /Col -->

						<div class="col-xs-12 weeks-meal-plan">
						<?php 
						$meals = get_field('week_Monday');
						if( $meals ): ?>
							<h3>Monday</h3>
							<ul>
							<?php foreach( $meals as $meals ): // variable must NOT be called $post (IMPORTANT) ?>
							    <li>
							    	<a href="<?php echo get_permalink( $meals->ID ); ?>">
							    		<article>
							    			<?php $category = get_the_category( $meals->ID ); ?> 
											<h3><?php echo $category[0]->cat_name; ?></h3>
							    			<img src="<?php the_field('recipe_image', $meals->ID); ?>" />
							    			<div class="recipe-details">
							    				<span><i class="fa fa-user"></i> <?php the_field('recipe_serves', $meals->ID); ?></span>
							    				<i class="fa fa-clock-o"></i> <?php the_field('recipe_prep_time', $meals->ID); ?>
							    			</div>
							    			<p><?php echo get_the_title( $meals->ID ); ?></p>
							    		</article>
							    		<a href="<?php echo get_permalink( $meals->ID ); ?>" class="cta cta-sm green">Preview</a>
							    	</a>
							    </li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>	
						</div> <!-- /Col -->
					
						<div class="col-xs-12 weeks-meal-plan">
						<?php 
						$meals = get_field('week_Tuesday');
						if( $meals ): ?>
							<h3>Tuesday</h3>
							<ul>
							<?php foreach( $meals as $meals ): // variable must NOT be called $post (IMPORTANT) ?>
							    <li>
							    	<a href="<?php echo get_permalink( $meals->ID ); ?>">
							    		<article>
							    			<?php $category = get_the_category( $meals->ID ); ?> 
											<h3><?php echo $category[0]->cat_name; ?></h3>
							    			<img src="<?php the_field('recipe_image', $meals->ID); ?>" />
							    			<div class="recipe-details">
							    				<span><i class="fa fa-user"></i> <?php the_field('recipe_serves', $meals->ID); ?></span>
							    				<i class="fa fa-clock-o"></i> <?php the_field('recipe_prep_time', $meals->ID); ?>
							    			</div>
							    			<p><?php echo get_the_title( $meals->ID ); ?></p>
							    		</article>
							    		<a href="<?php echo get_permalink( $meals->ID ); ?>" class="cta cta-sm green">Preview</a>
							    	</a>
							    </li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>	
						</div> <!-- /Col -->

						<div class="col-xs-12 weeks-meal-plan">
						<?php 
						$meals = get_field('week_Wednesday');
						if( $meals ): ?>
							<h3>Wednesday</h3>
							<ul>
							<?php foreach( $meals as $meals ): // variable must NOT be called $post (IMPORTANT) ?>
							    <li>
							    	<a href="<?php echo get_permalink( $meals->ID ); ?>">
							    		<article>
							    			<?php $category = get_the_category( $meals->ID ); ?> 
											<h3><?php echo $category[0]->cat_name; ?></h3>
							    			<img src="<?php the_field('recipe_image', $meals->ID); ?>" />
							    			<div class="recipe-details">
							    				<span><i class="fa fa-user"></i> <?php the_field('recipe_serves', $meals->ID); ?></span>
							    				<i class="fa fa-clock-o"></i> <?php the_field('recipe_prep_time', $meals->ID); ?>
							    			</div>
							    			<p><?php echo get_the_title( $meals->ID ); ?></p>
							    		</article>
							    		<a href="<?php echo get_permalink( $meals->ID ); ?>" class="cta cta-sm green">Preview</a>
							    	</a>
							    </li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>	
						</div> <!-- /Col -->

						<div class="col-xs-12 weeks-meal-plan">
						<?php 
						$meals = get_field('week_Thursday');
						if( $meals ): ?>
							<h3>Thursday</h3>
							<ul>
							<?php foreach( $meals as $meals ): // variable must NOT be called $post (IMPORTANT) ?>
							    <li>
							    	<a href="<?php echo get_permalink( $meals->ID ); ?>">
							    		<article>
							    			<?php $category = get_the_category( $meals->ID ); ?> 
											<h3><?php echo $category[0]->cat_name; ?></h3>
							    			<img src="<?php the_field('recipe_image', $meals->ID); ?>" />
							    			<div class="recipe-details">
							    				<span><i class="fa fa-user"></i> <?php the_field('recipe_serves', $meals->ID); ?></span>
							    				<i class="fa fa-clock-o"></i> <?php the_field('recipe_prep_time', $meals->ID); ?>
							    			</div>
							    			<p><?php echo get_the_title( $meals->ID ); ?></p>
							    		</article>
							    		<a href="<?php echo get_permalink( $meals->ID ); ?>" class="cta cta-sm green">Preview</a>
							    	</a>
							    </li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>	
						</div> <!-- /Col -->
						
						<div class="col-xs-12 weeks-meal-plan">
						<?php 
						$meals = get_field('week_Friday');
						if( $meals ): ?>
							<h3>Friday</h3>
							<ul>
							<?php foreach( $meals as $meals ): // variable must NOT be called $post (IMPORTANT) ?>
							    <li>
							    	<a href="<?php echo get_permalink( $meals->ID ); ?>">
							    		<article>
							    			<?php $category = get_the_category( $meals->ID ); ?> 
											<h3><?php echo $category[0]->cat_name; ?></h3>
							    			<img src="<?php the_field('recipe_image', $meals->ID); ?>" />
							    			<div class="recipe-details">
							    				<span><i class="fa fa-user"></i> <?php the_field('recipe_serves', $meals->ID); ?></span>
							    				<i class="fa fa-clock-o"></i> <?php the_field('recipe_prep_time', $meals->ID); ?>
							    			</div>
							    			<p><?php echo get_the_title( $meals->ID ); ?></p>
							    		</article>
							    		<a href="<?php echo get_permalink( $meals->ID ); ?>" class="cta cta-sm green">Preview</a>
							    	</a>
							    </li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>	
						</div> <!-- /Col -->

						<div class="col-xs-12 weeks-meal-plan">
						<?php 
						$meals = get_field('week_Saturday');
						if( $meals ): ?>
							<h3>Saturday</h3>
							<ul>
							<?php foreach( $meals as $meals ): // variable must NOT be called $post (IMPORTANT) ?>
							    <li>
							    	<a href="<?php echo get_permalink( $meals->ID ); ?>">
							    		<article>
							    			<?php $category = get_the_category( $meals->ID ); ?> 
											<h3><?php echo $category[0]->cat_name; ?></h3>
							    			<img src="<?php the_field('recipe_image', $meals->ID); ?>" />
							    			<div class="recipe-details">
							    				<span><i class="fa fa-user"></i> <?php the_field('recipe_serves', $meals->ID); ?></span>
							    				<i class="fa fa-clock-o"></i> <?php the_field('recipe_prep_time', $meals->ID); ?>
							    			</div>
							    			<p><?php echo get_the_title( $meals->ID ); ?></p>
							    		</article>
							    		<a href="<?php echo get_permalink( $meals->ID ); ?>" class="cta cta-sm green">Preview</a>
							    	</a>
							    </li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>	
						</div> <!-- /Col -->

					</div> <!-- /Row -->

				</div> <!-- /Col -->
			</div> <!-- /Row -->

			</div>

			<div class="tab-pane fade" id="exercise">
				
				<div class="row">
					<div class="col-xs-12">
						<?php the_field('weeks_exercise');?>
					</div> <!-- /Col -->
				</div> <!-- /Row -->

			</div>

		</div>
			
		</div> <!-- /Col -->
	</div> <!-- /Row -->
	
</div> <!-- /Container -->

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<hr>
		</div> <!-- /Col -->
		<div class="col-md-6 forum">
			<h2>Forum</h2>
			<?php bbp_get_template_part( 'bbpress/content', 'archive-topic' ); ?>
		</div> <!-- /Col -->
		<div class="col-md-6">
				<h2>Social feed</h2>
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</div> <!-- /Container -->

<?php get_sidebar(); ?>

<script>
	
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

</script>

<?php get_footer(); ?>