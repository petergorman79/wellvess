<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php wp_title( ' | ', true, 'right' ); ?></title>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,600,700' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="//use.typekit.net/xaw0ssb.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/includes/plugins/font-awesome-4.2.0/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/css/normalize.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/css/styles.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/plugins/jQuery.mmenu-master/src/css/jquery.mmenu.all.css">


<?php if (is_front_page() ) { ?>
<style>
/* xs */
@media (max-width: 768px) { 
	.hero {	background-image: url(<?php the_field('hero_image_xs');?>);
	background-repeat: no-repeat;
	background-position: bottom;
	background-size: cover; }
}
/* sm */
@media (min-width: 768px) {
	.hero {	background-image: url(<?php the_field('hero_image_sm');?>);
	background-repeat: no-repeat;
	background-position: bottom;
	background-size: cover; }
}
/* md */
@media (min-width: 992px) {
	.hero {	background-image: url(<?php the_field('hero_image_lg');?>);
	background-repeat: no-repeat;
	background-position: bottom;
	background-size: cover; }
}
</style>
<?php } else { ?>

<?php } ?>

<?php wp_head(); ?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/includes/js/respond.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/css/ie8-styles.css" />
<![endif]-->
</head>
<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1496438797268092&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(window).bind("load resize", function(){    
  var container_width = $('#container').width();    
    $('#container').html('<div class="fb-like-box" ' + 
    'data-href="https://www.facebook.com/news.com.au"' +
    ' data-width="' + container_width + '" data-show-faces="false" ' +
    'data-stream="true" data-header="true"></div>');
    FB.XFBML.parse( );    
}); 

</script>

<div id="wrapper" class="hfeed">

<div class="sticky">
<header>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<a  href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( get_bloginfo( 'name' ) ); ?>" rel="home">
					<img class="nav-logo" src="<?php bloginfo('template_directory'); ?>/img/wellvess.svg" alt="Wellvess - Arthritis Vitality Program" />
				</a>

				<nav class="desktop-nav hidden-xs hidden-sm">
					<ul>
						<?php if ( is_user_logged_in() ) {
						  wp_main_menu_no_ul( array( 'theme_location' => 'main-menu' ) ); ?>

					  	<?php } elseif (is_page('checkout')) { 
							 wp_focus_menu_no_ul( array( 'theme_location' => 'focus-menu' ) );
						} else { ?>

						<?php wp_main_menu_no_ul( array( 'theme_location' => 'main-menu' ) ); ?>
						<li class="main-login">
							<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login','woothemes'); ?>"><?php _e('Login','woothemes'); ?></a>
						</li>
						<?php } ?> 
					</ul>
				</nav>

				<a href="#my-menu" class="toggle-navigation visible-xs visible-sm"><i class="fa fa-bars"></i></a>
			</div> <!-- /Col -->
		</div> <!-- /Row -->
	</div> <!-- /Container -->


</header>
<nav id="my-menu" class="mobile-nav">
	
	<ul>
		<?php if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();?>
				<?php wp_member_menu_no_ul( array( 'theme_location' => 'member-menu' ) ); ?>
			  	<li>
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" id="my_account" title="<?php _e( 'My Account','woothemes'); ?>"><?php echo $current_user->user_firstname; ?>
					</a> 

					<ul>
						<?php wp_member_options_no_ul( array( 'theme_location' => 'member-options' ) ); ?>
					</ul>

				</li>


			  	<li class="mobile-logout">
			  		<a href="<?php echo wp_logout_url( home_url() ); ?>" title="Logout">Logout</a>
			  	</li>
				<?php wp_main_menu_no_ul( array( 'theme_location' => 'main-menu' ) ); ?>
			  	<li><a href="#my-page">Close menu</a></li>

		  	<?php } elseif (is_page('checkout')) { 
				 wp_focus_menu_no_ul( array( 'theme_location' => 'focus-menu' ) );

			} else { ?>
			<?php wp_main_menu_no_ul( array( 'theme_location' => 'main-menu' ) ); ?>
			<li class="mobile-login">
				<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login','woothemes'); ?>"><?php _e('Login','woothemes'); ?></a>
			</li>
			<li class="mobile-close"><a href="#my-page">Close menu</a></li>
		<?php } ?> 
	</ul>
      
</nav>

<?php if ( is_user_logged_in() ) {
$current_user = wp_get_current_user(); ?>

<div class="wrapper member-nav hidden-xs hidden-sm">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<nav class="members">
					<ul class="member-only">
						<?php wp_member_menu_no_ul( array( 'theme_location' => 'member-menu' ) ); ?>
					</ul>

					<ul class="profile-links">

						<li class="member-account-menu">
							<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" id="my_account" title="<?php _e('My Account','woothemes'); ?>"><?php echo $current_user->user_firstname; ?> <i class="fa fa-caret-down"></i>
							</a> 

							<ul class="member-sub-menu" style="display: none;">
								<?php wp_member_options_no_ul( array( 'theme_location' => 'member-options' ) ); ?>
								<li>
						  		<a href="<?php echo wp_logout_url( home_url() ); ?>" class="" title="Logout">Logout</a></li>
							</ul>

						</li>

					</ul>
				</nav>
			</div> <!-- /Col -->
		</div> <!-- /Row -->
	</div> <!-- /Container -->
</div>
						
<?php } else {

} ?>

</div> <!-- /Sticky -->
<?php if ( is_front_page() ) { ?>
<div class="pushdown"></div>
<?php } elseif ( is_user_logged_in() ) { ?>
<div class="pushdown-member"></div>
<?php } else { ?>
<div class="pushdown"></div>
<?php } ?>
