<?php 

/*
 * Template Name: Join Wellvess
 */

 get_header(); ?>


<div class="wrapper sub c1">
	<div class="container join-today">
		<div class="row">
			<div class="col-xs-12">
				<h1><?php the_field('page_title'); ?></h1>
				<?php the_field('sub_title'); ?>
			</div> <!-- /Col -->
		</div> <!-- /Row -->
	 	<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
	 			<div class="products">
					<?php echo do_shortcode( '[recent_products per_page="2" order="asc"]' ) ?>
				</div>
			</div> <!-- /Col -->
		</div> <!-- /Row -->
 	</div> <!-- /Container -->
</div> <!-- /Wrapper -->

<div class="next-program join" style="display:<?php the_field('program_date'); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
					<h4>Next program starts 6 October 2014</h4>
			</div> <!-- /Col -->
		</div> <!-- /Row -->
	</div> <!-- /Container -->
</div>


<!-- Section 3 - Who is it for -->

<div class="wrapper">
<section class="container section-3 join">
	<div class="row">
		<div class="col-md-6">
			<?php the_field('additional_content'); ?>
		</div> <!-- /Col -->
		<div class="col-md-6 hidden-xs hidden-sm">


			<?php $imageID = get_field('join_image'); ?>
			<?php $image = wp_get_attachment_image_src( $imageID, 'full' ); ?>
			<?php $alt_text = get_post_meta($imageID , '_wp_attachment_image_alt', true); ?>
			<span><img src="<?php echo $image[0]; ?>" alt="<?php echo $alt_text; ?>" /></span>
			
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
</div> <!-- /Wrapper -->
<!-- /Section 3 -->

<!-- Section 6 - Testimonial -->
<div class="wrapper c4 b-top b-bottom" style="display:<?php the_field('display_quote'); ?>">
<section class="container section-6">
	<div class="row">
		
		<div class="col-xs-12 col-sm-9 col-sm-push-3 col-md-8">
			
				<?php the_field('quote'); ?>
			
				<small><?php the_field('quote_details'); ?></small>
		</div> <!-- /Col -->
		<div class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-pull-9 col-sm-offset-0 col-md-offset-1">
			<?php $imageID = get_field('quote_image'); ?>
			<?php $image = wp_get_attachment_image_src( $imageID, 'full' ); ?>
			<?php $alt_text = get_post_meta($imageID , '_wp_attachment_image_alt', true); ?>
			<img src="<?php echo $image[0]; ?>" alt="<?php echo $alt_text; ?>" />
		</div> <!-- /Col -->
	</div> <!-- /Row -->
</section> <!-- /Container -->
</div> <!-- /Wrapper -->
<!-- /Section 6 -->

<?php get_footer(); ?>